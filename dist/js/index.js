$(document).ready(function () {
	/*
	|--------------------------------------------------------------------------
	| Perfil del usuario
	|--------------------------------------------------------------------------
	|
	| Control de campos del usuario
	|
	*/

	$("#nombre1").keyup(function() {
		var nombre1 = $("#nombre1").val();
		var ruta = "user=&nombre1="+nombre1;
		$.ajax({
			type: 'POST',
			url: 'zombies/this/index.php',
			data: ruta
		});
	});

	$("#nombre2").keyup(function() {
		var nombre2 = $("#nombre2").val();
		var ruta = "user=&nombre2="+nombre2;
		$.ajax({
			type: 'POST',
			url: 'zombies/this/index.php',
			data: ruta
		});
	});

	$("#apellido1").keyup(function() {
		var apellido1 = $("#apellido1").val();
		var ruta = "user=&apellido1="+apellido1;
		$.ajax({
			type: 'POST',
			url: 'zombies/this/index.php',
			data: ruta
		});
	});

	$("#apellido2").keyup(function() {
		var apellido2 = $("#apellido2").val();
		var ruta = "user=&apellido2="+apellido2;
		$.ajax({
			type: 'POST',
			url: 'zombies/this/index.php',
			data: ruta
		});
	});

	$("#region").on('click', function() {
		var region = $("#region").val();
		var ruta = "user=&region="+region;
		$.ajax({
			type: 'POST',
			url: 'zombies/this/index.php',
			data: ruta
		});
	});

	$("#cargo").keyup(function() {
		var cargo = $("#cargo").val();
		var ruta = "user=&cargo="+cargo;
		$.ajax({
			type: 'POST',
			url: 'zombies/this/index.php',
			data: ruta
		});
	});
});