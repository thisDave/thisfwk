<?php

require_once "../../app/config/config.php";

/*
|--------------------------------------------------------------------------
| Perfil del usuario
|--------------------------------------------------------------------------
|
| Control de campos en perfil de usuario
|
*/

if (isset($_POST['user']))
{
	$nombre1 = (isset($_POST['nombre1'])) ? preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($_POST['nombre1'])) : null;
	$nombre2 = (isset($_POST['nombre2'])) ? preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($_POST['nombre2'])) : null;
	$apellido1 = (isset($_POST['apellido1'])) ? preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($_POST['apellido1'])) : null;
	$apellido2 = (isset($_POST['apellido2'])) ? preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($_POST['apellido2'])) : null;
	$region = (isset($_POST['region'])) ? preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($_POST['region'])) : null;
	$cargo = (isset($_POST['cargo'])) ? preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($_POST['cargo'])) : null;

	$_SESSION['log']['nombre1'] = (!is_null($nombre1)) ? $nombre1 : $_SESSION['log']['nombre1'];
	$_SESSION['log']['nombre2'] = (!is_null($nombre2)) ? $nombre2 : $_SESSION['log']['nombre2'];
	$_SESSION['log']['apellido1'] = (!is_null($apellido1)) ? $apellido1 : $_SESSION['log']['apellido1'];
	$_SESSION['log']['apellido2'] = (!is_null($apellido2)) ? $apellido2 : $_SESSION['log']['apellido2'];
	$_SESSION['log']['region'] = (!is_null($region)) ? $region : $_SESSION['log']['region'];
	$_SESSION['log']['cargo'] = (!is_null($cargo)) ? $cargo : $_SESSION['log']['cargo'];

	$name = $_SESSION['log']['nombre1']." ".$_SESSION['log']['nombre2'];
	$lastname = $_SESSION['log']['apellido1']." ".$_SESSION['log']['apellido2'];
	$_SESSION['log']['nombreCompleto'] = $name." ".$lastname;
}