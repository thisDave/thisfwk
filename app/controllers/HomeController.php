<?php

require_once 'Controller.php';

class HomeController extends Controller
{
	public function reqviews($req, $val = null)
	{
		$_SESSION['view'] = $req;

		switch ($req)
		{
			case 'home':
				unset($_SESSION['view']);
			break;

			case 'logout':
				parent::salida($_SESSION['log']['id']);
				session_destroy();
			break;

			default:
				if (is_null($val))
					unset($_SESSION['val']);
				else
					$_SESSION['val'] = $val;
			break;
		}

		load_view();
	}

	private function get_thumbnail_binary($pictures)
	{
		$binarios = [];

		foreach ($pictures['tempName'] as $key => $value)
		{
			require_once "tools/Resize/Resize.php";

			/******* Extraemos el valor binario de la imagen original *******/

	        $fp = fopen($pictures['tempName'][$key], "r");
	        $picture_binary = addslashes(fread($fp, $pictures['fileSize'][$key]));
	        fclose($fp);

	        /******* Guardamos la img temporalmente *******/

	        $folder = "picTemp/";

	        if (!is_dir($folder)) { mkdir($folder, 0777, true); }

            $extension = explode('.', $pictures['fileName'][$key]);
            $pictures['fileName'][$key] = $this->getKey(5).".".$extension[1];

	        $destino = $folder.$pictures['fileName'][$key];
	        opendir($folder);
	        move_uploaded_file($pictures['tempName'][$key], $destino);

	        /******* Creamos el thumbnail de la img *******/

	        $resizeObj = new Resize("picTemp/".$pictures['fileName'][$key]);
	        $resizeObj -> resizeImage(128, 128, 'crop');
	        $location = 'picTemp/thumbnail_'.$this->getKey(5).'.jpg';
	        $resizeObj -> saveImage($location, 100);
	        $thumbnail_binary = addslashes(file_get_contents($location));

	        /******* Eliminamos las imagenes y el directorio temporal *******/

	        unlink($destino);
	        unlink($location);
	        rmdir($folder);

	        $binarios['picture_binary'][] = $picture_binary;
	        $binarios['picture_type'][] = $pictures['fileType'][$key];
	        $binarios['thumbnail_binary'][] = $thumbnail_binary;
		}

		return $binarios;
	}

	private function get_binary($pictures)
	{
		$binarios = [];

		foreach ($pictures['tempName'] as $key => $value)
		{
			/******* Extraemos el valor binario de la imagen original *******/

	        $fp = fopen($pictures['tempName'][$key], "r");
	        $picture_binary = addslashes(fread($fp, $pictures['fileSize'][$key]));
	        fclose($fp);

	        /******* Guardamos el binario y su tipo *******/

	        $binarios['picture_binary'][] = $picture_binary;
	        $binarios['picture_type'][] = $pictures['fileType'][$key];
		}

		return $binarios;
	}

	private function save_file($folder, $files)
	{
		//Armamos la carpeta destino
		$dir = "files/{$folder}/";

		//Verificamos si la ruta existe
		if (!is_dir($dir)) { mkdir($dir, 0777, true); }

		//Extraemos la extensión del archivo
		$extension = explode('.', $files['file']['name']);

		//Cambiamos el nombre del archivo
		$files['file']['name'] = "file.".$extension[1];

		//Verificamos si el archivo existe en la ruta establecida
		if (file_exists($dir.$files['file']['name'])) { unlink($dir.$files['file']['name']); }

		//Guardamos la ruta completa de la imagen
		$route = $dir.$files['file']['name'];

		//Abrimos el directorio
		opendir($dir);

		//Subimos la imagen y retornamos su resultado
		if (move_uploaded_file($files['file']['tmp_name'], $route))
			return true;
		else
			return false;
	}

	public function updtUser()
	{
		$datos = [
			$_SESSION['log']['nombre1'],
			$_SESSION['log']['nombre2'],
			$_SESSION['log']['apellido1'],
			$_SESSION['log']['apellido2'],
			$_SESSION['log']['region'],
			$_SESSION['log']['cargo']
		];

		$x = true;

		foreach ($datos as $value) { if (empty($value)) { $x = false; break; } }

		if ($x)
		{
			if (parent::actualizarUsuario())
			{
				$_SESSION['sweetAlert']['icon'] = 'success';
				$_SESSION['sweetAlert']['text'] = 'Datos del usuario actualizados exitosamente';
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = 'error';
				$_SESSION['sweetAlert']['text'] = 'No fue posible conectarse con la base de datos';
			}
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'Se encontraron datos vacíos en la solicitud.';
		}

		unset($_SESSION['updateInfoUser']);

		load_view();
	}

	public function updtPic($idFoto)
	{
		if (parent::updtPicProfile($idFoto))
		{
			$info = parent::infoUsuario($_SESSION['log']['id']);
        	$_SESSION['log']['foto'] = $info['foto'];
			$_SESSION['sweetAlert']['icon'] = 'success';
			$_SESSION['sweetAlert']['text'] = 'Foto de perfil actualizada';
		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'No se pudo conectar a la base de datos';
		}

		load_view();
	}

	public function updatePass($currentPass, $pass, $password)
	{
		if (parent::validaPassword($currentPass))
		{
			if (strlen($pass) >= 8 && strlen($password) >= 8)
			{
				if ($pass == $password)
				{
					$arr_pass = str_split($pass);

					$banco = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789abcdefghijklmnñopqrstuvwxyz_@-$!';

					$arr_banco = str_split($banco);

					$x = true;

					foreach ($arr_pass as $valor_pass) { $x = (!in_array($valor_pass, $arr_banco)) ? false : true; }

					if ($x)
					{
						$password = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 12]);

						if (parent::updatePassword($password)) {
							$_SESSION['sweetAlert']['icon'] = 'success';
							$_SESSION['sweetAlert']['text'] = 'Contraseña actualizada exitosamente';
						}
						else {
							$_SESSION['sweetAlert']['icon'] = 'error';
							$_SESSION['sweetAlert']['text'] = 'No fue posible conectarse con la base de datos.';
						}
					}
				}
				else
				{
					$_SESSION['sweetAlert']['icon'] = 'error';
					$_SESSION['sweetAlert']['text'] = 'Las contraseñas no coinciden';
				}
			}
			else
			{
				$_SESSION['sweetAlert']['icon'] = 'error';
				$_SESSION['sweetAlert']['text'] = 'La longitud de las contraseñas son incorrectas';
			}

		}
		else
		{
			$_SESSION['sweetAlert']['icon'] = 'error';
			$_SESSION['sweetAlert']['text'] = 'La contraseña actual es incorrecta';
		}

		header("Location:".URL);
	}

	public function support($asunto, $comentario)
	{
		$user = parent::infoUsuario($_SESSION['log']['id']);

		$asunto = preg_replace('([^A-Za-z0-9 ])', '', trim($asunto));
		$comentario = preg_replace('([^A-Za-z0-9 ])', '', trim($comentario));

		if (!empty($asunto) && !empty($comentario))
		{
			$html = '
			<!DOCTYPE html>
			<html lang="es-SV">
			    <head>
			        <meta charset="utf-8">
			        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
			        <title>fleetSys</title>
			    </head>
			    <body>
			        <div class="container-fluid">
			            <div class="row">
			                <div class="col-12">
			                    <div class="container">
			                        <div class="row">
			                            <div class="col-12">
			                                <h3>Has recibido una solicitud de soporte en FleetSys</h3>
			                            </div>
			                        </div>
			                        <div class="row mt-3">
			                            <div class="col-12">
			                                <p>
			                                   A continuación te presentamos los datos del usuario:
			                                </p>
			                                <p>
			                                    <strong>Nombre:</strong> '.$user['nombreCompleto'].'<br> <br>
			                                    <strong>Email:</strong> '.$user['email'].' <br> <br>
			                                    <strong>Asunto del problema:</strong> '.$asunto.' <br> <br>
			                                    <strong>Comentarios:</strong> <br> '.$comentario.'
			                                </p>
			                                <p>
			                                    Atentamente:<br>
			                                    <strong>Sistemas Educo El Salvador</strong>
			                                </p>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </body>
			</html>
			';

			$_SESSION['alertResult'] = (parent::sendMail('isaac.ramos@educo.org', 'Soporte FleetSys', $html)) ? true : false;
		}
		else
		{
			$_SESSION['alertResult'] = false;
		}

		unset($_SESSION['asunto']);
		unset($_SESSION['comentario']);
		unset($_SESSION['progressBar']);

		load_view();
	}

	public function showComments()
	{
		$comments = parent::getComments();

		if ($comments)
		{
			foreach ($comments['idComentario'] as $key => $value)
			{
				echo '
				<div class="direct-chat-msg">
					<div class="direct-chat-infos clearfix">
						<span class="direct-chat-name float-left">
						'.$_SESSION['log']['nombre1'].' '.$_SESSION['log']['apellido1'].'
						</span>
						<span class="direct-chat-timestamp float-right">
						'.date_format(date_create($comments['fecha'][$key]), 'd-M-Y').'
						'.$comments['hora'][$key].'
						</span>
					</div>
					<img class="direct-chat-img" src="data:image/png;base64,'.$_SESSION['log']['foto'].'">
					<div class="direct-chat-text">
						'.$comments['comentario'][$key].'
					</div>';

				if ($_SESSION['log']['id'] == $comments['idUsuario'][$key])
				{
					echo '
					<a href="#" class="text-sm text-blue" data-toggle="modal" data-target="#proximamente">Editar</a>
					<a href="'.URL.'?delComment='.$value.'" class="text-sm text-danger">Eliminar</a>';
				}
				echo '</div>';
			}
		}
	}

	public function upInfo($val)
	{
		if ($val === 'on')
			$_SESSION['updateInfoUser'] = true;
		else
			unset($_SESSION['updateInfoUser']);

		load_view();
	}

	public function logout()
	{
		session_destroy();
		load_view();
	}
}

$objHome = new HomeController;

if (isset($_GET['req']))
{
	if (isset($_GET['val']))
		$objHome->reqviews($_GET['req'], $_GET['val']);
	else
		$objHome->reqviews($_GET['req']);
}

if (isset($_POST['update_password']))
{
	$objHome->updatePass($_POST['currentPass'], $_POST['pass'], $_POST['password']);
}

if (isset($_POST['soporte']))
{
	$_SESSION['asunto'] = $_POST['asunto'];
	$_SESSION['comentario'] = $_POST['comentario'];
	$_SESSION['progressBar'] = true;

	load_view();
}

if (isset($_GET['sendSupport']))
{
	$objHome->support($_SESSION['asunto'], $_SESSION['comentario']);
}

if (isset($_POST['newComment']))
{
	$model->insertComment(preg_replace('([^A-Za-zÁ-ź0-9-.¡!:\) ])', '', trim($_POST['comment'])), $_SESSION['log']['id']);

	load_view();
}

if (isset($_GET['delComment']))
{
	$model->delComment(preg_replace('([^A-Za-z0-9- ])', '', trim($_GET['delComment'])), $_SESSION['log']['id']);

	load_view();
}