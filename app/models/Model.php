<?php

require_once APP.'/config/Conection.php';

class Model extends Conection
{
	public function showRes($query)
	{
		$c = parent::conectar();
		$c->set_charset('utf8');
		$res = $c->query($query);
        parent::desconectar();
		return $res;
	}

	public function logInfo($email, $pass)
	{
		$resultado = $this->showRes("SELECT * FROM tbl_usuarios WHERE email = '{$email}'");

        $fila = $resultado->fetch_assoc();

	    if (password_verify($pass, $fila['pass']))
	    	$id = $fila['idUsuario'];
        else
            $id = false;

		if ($id)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');
			$stm = $conexion->prepare("call SP_login(?);");
			$stm->bind_param('i', $id);
			$stm->execute();

   			$stm->bind_result(
   				$idUsuario,
		        $nivel
   			);

			if (!empty($stm))
			{
				while($stm->fetch())
				{
                    $resultado = $this->showRes("SELECT * FROM `tbl_entradas` WHERE idUsuario = {$idUsuario}");

                    if ($resultado->num_rows < 1)
                    {
                        return 'firstIn';
                    }
                    else
                    {
    					$this->showRes("INSERT INTO `tbl_entradas`(`idUsuario`) VALUES ({$idUsuario})");

                        $_SESSION['log'] = $this->infoUsuario($idUsuario);
                    }
				}

				$stm->close();

				return true;
			}
			else
			{
				$stm->close();

				return false;
			}
		}
		else
		{
			return false;
		}
	}

    public function salida($id)
    {
        $this->showRes("INSERT INTO `tbl_salidas`(`idUsuario`) VALUES ({$id})");
    }

	public function infoUsuario($idUsuario)
    {
    	$resultado = $this->showRes("CALL SP_infoUsuario(".$idUsuario.")");

    	if ($resultado->num_rows > 0)
		{
			$info = [];

			while ($fila = $resultado->fetch_assoc())
			{
                $info['id'] = $fila['idUsuario'];
				$info['nombre1'] = $fila['firstName'];
				$info['nombre2'] = $fila['secndName'];
                $info['apellido1'] = $fila['firstApe'];
                $info['apellido2'] = $fila['secndApe'];
				$info['email'] = $fila['email'];
				$info['level'] = $fila['Nivel'];
				$info['cargo'] = $fila['Cargo'];
				$info['region'] = $fila['Region'];
				$info['pais'] = $fila['Pais'];
                $info['foto'] = base64_encode($fila['foto']);
                $info['estado'] = $fila['estado'];
			}

            $info['nombreCompleto'] = $info['nombre1']." ".$info['nombre2']." ".$info['apellido1']." ".$info['apellido2'];

			return $info;
		}
		else
		{
			return false;
		}

		$resultado->free();

		parent::desconectar();
    }

    public function actualizarUsuario()
    {
        $id = $_SESSION['log']['id'];
        $nombre1 = $_SESSION['log']['nombre1'];
        $nombre2 = $_SESSION['log']['nombre2'];
        $apellido1 = $_SESSION['log']['apellido1'];
        $apellido2 = $_SESSION['log']['apellido2'];
        $region = $this->getIdRegion($_SESSION['log']['region']);
        $cargo = $_SESSION['log']['cargo'];

        $query = "CALL SP_actualizarUsuario('{$nombre1}', '{$nombre2}', '{$apellido1}', '{$apellido2}', '{$cargo}', $region, $id)";

    	$resultado = $this->showRes($query);

    	if ($resultado)
    		return true;
    	else
    		return false;
    }

	public function setCookieToken($email, $pass, $token)
	{
		$resultado = $this->showRes("INSERT INTO tbl_cookies VALUES ('".$email."', '".$pass."', '".$token."')");

		if($resultado)
			return true;
		else
			return false;
	}

	public function getCookieToken($token)
	{
		$resultado = $this->showRes("SELECT email, pass FROM tbl_cookies WHERE sessionToken = '".$token."'");

		$info = [];

		while ($campo = $resultado->fetch_assoc())
		{
		    $info['usuario'] = $campo['email'];
		    $info['pass'] = $campo['pass'];
		}

		if($resultado)
			return $info;
		else
			return false;
	}

	public function setResetToken($email, $key)
	{
		$resultado = $this->showRes("UPDATE tbl_usuarios SET token = '".$key."', fechaToken = NOW() WHERE email = '".$email."'");

		if ($resultado)
			return true;
		else
			return false;
	}

	public function validarToken($token)
	{
		if (strlen($token) == 50)
		{
			$resultado = $this->showRes("SELECT * FROM tbl_usuarios WHERE token = '".$token."'");

			if ($resultado->num_rows > 0)
				return true;
			else
				return false;
		}
	}

	public function recoverPassword($password, $token)
	{
        $res = $this->showRes("SELECT idUsuario FROM tbl_usuarios WHERE token = '{$token}'");

        if ($res->num_rows > 0)
        {
            $dato = $res->fetch_assoc();

            $id = $dato['idUsuario'];

            $this->showRes("INSERT INTO `tbl_entradas`(`idUsuario`) VALUES ({$id})");

            $token = $this->showRes("
                UPDATE
                    tbl_usuarios
                SET
                    fechaToken = NULL,
                    pass = '{$password}',
                    token = ''
                WHERE
                    idUsuario = {$id}");

    		if ($token)
    			return true;
    		else
    			return false;
        }
        else
        {
            return false;
        }
	}

	public function validaPassword($currentPass)
	{
		$resultado = $this->showRes("SELECT * FROM tbl_usuarios");

		$x = false;

		while ($fila = $resultado->fetch_assoc())
		{
		    if ($fila['idUsuario'] == $_SESSION['log']['id'] && password_verify($currentPass, $fila['pass']))
		    {
		    	$x = true;
		    	break;
		    }
		}

		return $x;
	}

	public function updatePassword($password)
	{
		$query = "
		UPDATE
			tbl_usuarios
		SET
			pass = '".$password."'
		WHERE
			idUsuario = ".$_SESSION['log']['id'];

		$resultado = $this->showRes($query);

		if ($resultado)
			return true;
		else
			return false;
	}

	public function listarPaises()
    {
        $paises = [];

        $paises = $this->showRes("SELECT * FROM tbl_Paises");

        while ($data = $paises->fetch_assoc()) {
            $paises['id'][] = $data['idPais'];
            $paises['pais'][] = $data['Pais'];
        }

        return $paises;
    }

	public function getIdPais($pais)
    {
        $pais = $this->showRes("SELECT idPais FROM tbl_Paises WHERE pais = '".$pais."'");

        $data = $pais->fetch_assoc();

        $idPais = $data['idPais'];

        return $idPais;
    }

    public function getIdRegion($region)
    {
        $pais = $this->showRes("SELECT idRegion FROM tbl_Regiones WHERE region = '".$region."'");

        $data = $pais->fetch_assoc();

        $idPais = $data['idRegion'];

        return $idPais;
    }

	public function listarRegiones($idPais)
    {
        $regiones = [];

        $resultado = $this->showRes("SELECT * FROM tbl_Regiones WHERE idPais = ".$idPais);

        while ($data = $resultado->fetch_assoc())
        {
            $regiones['id'][] = $data['idRegion'];
            $regiones['region'][] = $data['region'];
        }

        return $regiones;
    }

    public function profilePics()
    {
        $resultado = $this->showRes("SELECT * FROM tbl_fotoPerfiles");

        if ($resultado)
        {
            $fotos = [];

            while ($data = $resultado->fetch_assoc())
            {
                $fotos['idFoto'][] = $data['idFoto'];
                $fotos['nombre'][] = $data['nombre'];
                $fotos['formato'][] = $data['formato'];
                $fotos['foto'][] = base64_encode($data['foto']);
            }

            return $fotos;
        }
        else
        {
            return false;
        }
    }

    public function updtPicProfile($idFoto)
    {
        $resultado = $this->showRes("UPDATE tbl_usuarios SET idFoto = {$idFoto} WHERE idUsuario = {$_SESSION['log']['id']}");

        if ($resultado)
            return true;
        else
            return false;
    }

    public function listarEstados()
    {
        $resultado = $this->showRes("SELECT * FROM tbl_Estados");

        if ($resultado->num_rows > 0)
        {
            while($data = $resultado->fetch_assoc())
            {
                $estados['idEstado'][] = $data['idEstado'];
                $estados['estado'][] = $data['Estado'];
            }

            return $estados;
        }
        else
        {
            return false;
        }
    }

    public function listaUsuarios()
    {
        $resultado = $this->showRes("CALL SP_listaUsuarios()");

        if ($resultado)
        {
            if ($resultado->num_rows > 0)
            {
                while ($data = $resultado->fetch_assoc())
                {
                    $lista['idUsuario'][] = $data['idUsuario'];
                    $lista['nombre1'][] = $data['nombre1'];
                    $lista['nombre2'][] = $data['nombre2'];
                    $lista['apellido1'][] = $data['apellido1'];
                    $lista['apellido2'][] = $data['apellido2'];
                    $lista['nombreCompleto'][] = $data['nombreCompleto'];
                    $lista['region'][] = $data['region'];
                    $lista['email'][] = $data['email'];
                    $lista['cargo'][] = $data['cargo'];
                    $lista['idEstado'][] = $data['idEstado'];
                    $lista['estado'][] = $data['estado'];
                }

                return $lista;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function insertComment($comment, $idUsuario)
    {
        if (!empty($comment))
        {
            $query = "INSERT INTO tbl_comentarioSistema VALUES (NULL, ".$idUsuario.", '".$comment."', CURDATE(), TIME_FORMAT(NOW(), '%H:%i'))";
            $resultado = $this->showRes($query);
        }
    }

    public function delComment($idComment, $idUsuario)
    {
        $res = $this->showRes("SELECT * FROM tbl_comentarioSistema WHERE idComentario = ".$idComment." AND idUsuario = ".$idUsuario);

        if ($res && $res->num_rows > 0)
        {
            $resultado = $this->showRes("DELETE FROM tbl_comentarioSistema WHERE idComentario = ".$idComment);
        }
    }

    public function getComments()
    {
        $resultado = $this->showRes("SELECT * FROM tbl_comentarioSistema ORDER BY idComentario DESC");

        if ($resultado)
        {
            if ($resultado->num_rows > 0)
            {
                while ($data = $resultado->fetch_assoc())
                {
                    $comments['idComentario'][] = $data['idComentario'];
                    $comments['idUsuario'][] = $data['idUsuario'];
                    $comments['comentario'][] = $data['comentario'];
                    $comments['fecha'][] = $data['fecha'];
                    $comments['hora'][] = $data['hora'];
                }

                return $comments;
            }
            else
            {
                return false;
            }
        }
    }
}