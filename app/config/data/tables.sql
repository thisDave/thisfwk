/*
----------------------------------------
|         CREACIÓN Y SELECCION         |
----------------------------------------
*/

DROP DATABASE IF EXISTS `db_thisFramework`;

CREATE DATABASE IF NOT EXISTS `db_thisFramework` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `db_thisFramework`;

/*
----------------------------------------
|          TABLAS PRINCIPALES          |
----------------------------------------
*/


/*TABLA Paises*/

CREATE TABLE IF NOT EXISTS tbl_Paises(
	idPais	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Pais 	VARCHAR(45) NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*
TABLA Regiones
La tabla regiones incluye la foránea a pais, por lo que no es necesario que se mande a llamar
el pais de un usuario desde la tabla anterior
*/


CREATE TABLE IF NOT EXISTS tbl_Regiones(
	idRegion	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idPais 		INT NOT NULL,
	region		VARCHAR(45) NOT NULL,

	CONSTRAINT FK_regiones_idPais FOREIGN KEY (idPais) REFERENCES tbl_Paises (idPais)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Niveles de Usuario*/

CREATE TABLE IF NOT EXISTS tbl_nivelesUsuarios(
  idNivelUsuario	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nivelUsuario		VARCHAR(20) NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* TABLA Estados */

CREATE TABLE IF NOT EXISTS tbl_Estados(
	idEstado	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Estado		VARCHAR(25) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Fotos de Perfil*/

CREATE TABLE IF NOT EXISTS tbl_fotoPerfiles(
	idFoto		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nombre		VARCHAR(15) NOT NULL,
	formato		VARCHAR(12) NOT NULL,
	foto 		BLOB NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Usuarios*/

CREATE TABLE IF NOT EXISTS tbl_usuarios(
	idUsuario		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	firstName 		VARCHAR(45) NOT NULL,
    secndName		VARCHAR(45) NOT NULL,
	firstApe 		VARCHAR(45) NOT NULL,
    secndApe		VARCHAR(45) NOT NULL,
	idNivelUsuario 	INT NOT NULL,
	idRegion 		INT NOT NULL,
	email 			VARCHAR(70) NOT NULL,
	pass			VARCHAR(100) NOT NULL,
	cargo			VARCHAR(70) NOT NULL,
	token 			VARCHAR(70) NULL,
	fechaToken 		DATETIME NULL,
    idFoto			INT NOT NULL DEFAULT 1,
    idEstado		INT NOT NULL,

	CONSTRAINT FK_usuarios_idRegion FOREIGN KEY (idRegion) REFERENCES tbl_Regiones (idRegion),

	CONSTRAINT FK_usuarios_idNivelUsuario FOREIGN KEY (idNivelUsuario) REFERENCES tbl_nivelesUsuarios (idNivelUsuario),

    CONSTRAINT FK_usuarios_idFoto FOREIGN KEY (idFoto) REFERENCES tbl_fotoPerfiles (idFoto),

    CONSTRAINT FK_usuarios_idEstado FOREIGN KEY (idEstado) REFERENCES tbl_Estados (idEstado)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Histories*/

CREATE TABLE IF NOT EXISTS tbl_Histories(
	idHistory		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	fechaIngreso 	DATE NULL,
	horaInicio 		TIME NULL,
	horaFin 		TIME NULL,
	idUsuario 		INT NULL,

	CONSTRAINT FK_histories_idUsuario FOREIGN KEY (idUsuario) REFERENCES tbl_usuarios (idUsuario)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Cookies*/

CREATE TABLE IF NOT EXISTS tbl_cookies(
	email			VARCHAR(60) NOT NULL,
	pass			VARCHAR(60) NOT NULL,
    sessionToken	VARCHAR(125) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Comentarios*/

CREATE TABLE IF NOT EXISTS tbl_comentarioSistema(
	idComentario	INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idUsuario		INT NULL,
    comentario		VARCHAR(200),
    fecha			DATE,
    hora			TIME
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Registros de entrada y salidas */

CREATE TABLE IF NOT EXISTS tbl_entradas(
	idEntrada	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idUsuario	INT NOT NULL,
    fecha		DATETIME NOT NULL DEFAULT NOW(),

    CONSTRAINT FK_entradas_idUsuario FOREIGN KEY (idUsuario) REFERENCES tbl_usuarios (idUsuario)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS tbl_salidas(
	idEntrada	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idUsuario	INT NOT NULL,
    fecha		DATETIME NOT NULL DEFAULT NOW(),

    CONSTRAINT FK_salidas_idUsuario FOREIGN KEY (idUsuario) REFERENCES tbl_usuarios (idUsuario)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;