/*
-----------------------------------------
|       PROCEDIMIENTOS ALMACENADOS      |
-----------------------------------------
*/

USE `db_thisFramework`;

DELIMITER //
CREATE PROCEDURE SP_login( _id INT )
BEGIN
	SELECT
		u.idUsuario,
		n.nivelUsuario AS 'nivel'
	FROM
		tbl_usuarios u
	INNER JOIN
		tbl_nivelesUsuarios n ON u.idNivelUsuario = n.idNivelUsuario
	WHERE
		u.idUsuario = _id;
END //


DELIMITER //
CREATE PROCEDURE SP_infoUsuario( _id INT )
BEGIN
	SELECT
		u.idUsuario,
		u.firstName,
        u.secndName,
        u.firstApe,
        u.secndApe,
        u.email,
		n.nivelUsuario AS 'Nivel',
        u.Cargo,
        r.Region,
        p.Pais,
        f.foto,
        e.estado
	FROM
		tbl_usuarios u
	INNER JOIN
		tbl_nivelesUsuarios n ON u.idNivelUsuario = n.idNivelUsuario
	INNER JOIN
		tbl_Regiones r ON u.idRegion = r.idRegion
	INNER JOIN
		tbl_Paises p ON r.idPais = p.idPais
	INNER JOIN
		tbl_fotoPerfiles f ON u.idFoto = f.idFoto
	INNER JOIN
		tbl_Estados e ON u.idEstado = e.idEstado
	WHERE
		u.idUsuario = _id;
END //


DELIMITER //
CREATE PROCEDURE SP_actualizarUsuario(
    _firstName 	VARCHAR(45),
    _secndName 	VARCHAR(45),
    _firstApe 	VARCHAR(45),
    _secndApe 	VARCHAR(45),
    _cargo		VARCHAR(70),
    _idRegion	INT,
    _idUsuario	INT
)
BEGIN
	UPDATE
		tbl_usuarios
	SET
		firstName = _firstName,
        secndName = _secndName,
        firstApe = _firstApe,
        secndApe = _secndApe,
        Cargo = _cargo,
        idRegion = _idRegion
	WHERE
		idUsuario = _idUsuario;
END //


DELIMITER //
CREATE PROCEDURE SP_listaUsuarios()
BEGIN
	SELECT
		u.idUsuario,
		u.firstName AS 'nombre1',
		u.secndName  AS 'nombre2',
		u.firstApe  AS 'apellido1',
		u.secndApe  AS 'apellido2',
        CONCAT(u.firstName, ' ', u.secndName, ' ', u.firstApe, ' ', u.secndApe) AS 'nombreCompleto',
		r.region,
		u.email,
		u.cargo,
		u.idEstado,
        e.estado
	FROM
		tbl_usuarios u
	INNER JOIN
		tbl_Regiones r ON u.idRegion = r.idRegion
	INNER JOIN
		tbl_Estados e ON u.idEstado = e.idEstado;
END //