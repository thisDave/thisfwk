<?php require_once APP . "/views/master/header.php"; ?>

<?php require_once APP."/views/master/{$_SESSION['log']['level']}-nav.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Soporte técnico</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= URL ?>?req=home">Inicio</a></li>
              <li class="breadcrumb-item active">Soporte</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Escribe tus consultas aquí.</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= URL ?>" method="post" accept-charset="utf-8">
                <div class="card-body">
                  <div class="form-group">
                    <label for="asunto">Asunto</label>
                    <input type="text" class="form-control" name="asunto" id="asunto" placeholder="Asunto" required>
                  </div>
                  <div class="form-group">
                    <label for="coment">Comentarios</label>
                    <textarea class="form-control" name="comentario" id="coment" rows="3" placeholder="Escribe aqui tus comentarios." required></textarea>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" name="soporte" class="btn btn-primary">Enviar</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<?php require_once APP."/views/master/footer_js.php"; ?>

<?php if (isset($_SESSION['progressBar'])): ?>

<meta http-equiv="refresh" content="0.5;URL=<?= URL ?>?sendSupport=true">

<script>
  var Toast = Swal.mixin({
    toast: false,
    position: 'center',
    showConfirmButton: false,
    timer: 10000,
    timerProgressBar: true
  });

  Toast.fire({
    icon: 'info',
    title: 'Enviando mensaje de soporte, por favor espere...'
  });
</script>

<?php endif ?>

<?php if (isset($_SESSION['alertResult']) && $_SESSION['alertResult'] == true): ?>

<meta http-equiv="refresh" content="2;URL=<?= URL ?>?delalert=alertResult">

<script>
  var Toast = Swal.mixin({
    toast: false,
    position: 'center',
    showConfirmButton: false,
    timer: 2000
  });

  Toast.fire({
    icon: 'success',
    title: 'Información Actualizada.'
  });
</script>

<?php elseif (isset($_SESSION['alertResult']) && $_SESSION['alertResult'] == false): ?>

<meta http-equiv="refresh" content="2;URL=<?= URL ?>?delalert=alertResult">

<script>
  var Toast = Swal.mixin({
    toast: false,
    position: 'center',
    showConfirmButton: false,
    timer: 2000
  });

  Toast.fire({
    icon: 'error',
    title: 'Error en la solicitud.',
    text: 'Por favor ingrese datos válidos.'
  });
</script>

<?php endif ?>

<?php require_once APP."/views/master/footer_end.php"; ?>
